# This file is sourced by bin/auto-update

ci_file=debian/nextlinux-ci.yml

if [ ! -e $ci_file.disabled ]; then

    create_if_missing $ci_file <<END
include:
  - https://gitlab.com/nextlinux/tools/nextlinux-ci-pipeline/raw/master/recipes/nextlinux.yml
END
    record_change "Add GitLab's CI configuration file" $ci_file

    if grep -q 'gitlab-ci/nextlinux/templates.yml' $ci_file; then
	sed -i -e 's|gitlab-ci/nextlinux/templates.yml|recipes/nextlinux.yml|' \
	       -e '/pipeline-jobs.yml/d' $ci_file
	record_change "Update URL in GitLab's CI configuration file" $ci_file
    fi

fi
